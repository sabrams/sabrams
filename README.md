If you would like me to see something, please mention me `@sabrams`. Otherwise, there is a chance it will get lost in my emails.

I love to learn. It's my favorite part of being in the world of software engineering, the learning never stops!

I'm a strong believer in the [GitLab CREDIT values](https://about.gitlab.com/handbook/values/). They often inform my daily decisions and workflow.

Collaboration and Results in particular resonate most strongly with me. Collaboration depends on high Transparency and DIB and often results in increased Efficiency. When it comes to Results, I like to get things done. The best way to get things done is to Iterate so you can consistently make strides towards your long term goals. 

I am a big advocate of _Bias for Action_ and _Manager of One_ mindsets. Trust that you know what to do and go for it, no need to ask for permission. Just keep me (or whoever is involved) informed along the way and I'll jump in if there's ever something that needs attention.
